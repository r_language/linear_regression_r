***First Linear Regression***

**Question 1**

Simulate n data and visualize.

**Question 2**

Estimate the model's coefficients of the first question with lower square method.

**Question 3**

Overlay the estimate regression straight line on the graphic.

***Prediction and residuals***

**Question 4**

Calculate the predicted values and visualize the adjustement quality (graphics y vs hat y).

**Question 5**

Calculate the residuals hat ei and test the residuals' normality.
Represent graphically the residual's distribution.

**Question 6**

Check the homoscedasticity hypothesis (constant variance of the residuals).

**Question 7**

Calculate the error variance of the model : s squared = 1/2-n sum of hat ei squared.

**Question 8**

Obtain the same results with lm function.

**Question 9**

Compare with calculation by hand.

***Confidence Intervals***

**Question 10**

Check the dependancy between coefficients.
Calculate the covariance between estimated coefficients.

**Question 11**

Calculate the confidence intervals of the estimators beta 0 and beta 1 thanks to the confint function and by hand.

***Adjustment quality***

**Question 12**
Calculate R2 (check the property R2 = corr2(x, y)).

**Question 13**

Calculate the confidence interval and prediction interval for y.

**Question 14**

Calculate the PRESS. Compare with the result obtained thanks to PRESS function from MPV package.


**Question 15**
Analyse the residuals of the model.


**Question 16**

Analyze the abnormal observations.

***Property of the estimators***

Realize simulations and analyze the distribution of the estimated parameters.


**Question 17**

Check the distribution of beta 0 and beta 1.

**Question 18**

Check the distribution of S squared hat.
S squared : residual variance estimated, squared sigma : real residual variance.

***Second Linear Regression***

- Objective : Predict the systolic arterial pressure (Y) depending on the different parameters.
 
**Question 1**

Load the file.

**Question 2**
Estimate the following model : Interpret the results of SBP_50 = β0 + β1AGE_50 + β2DBP_50 + β3HT_50 + β4WT_50 + β5CHOL_50 + β6SES.

**Question 3**

Use the step function to select automatically the variables of interest in the model and interpret the result.

**Question 4**

Check the absence of multicolinearity.

**Question 5**

Compare model 2 and model 1.

**Question 6**

Evaluate the adjustment quality of the model 2. Visualize the quality of prediction.

**Question 7**

Analyse the residuals and the influent points of the model 2.

**Question 8**

Analyze the influential points by hand.

**Question 9**

Analyze without the influential points. Interpret the coefficients of the model 3 obtained (we call the result fit3).

**Question 10**

Calculate the confidence interval and the prediction interval of the model 3.
Compare the PRESS in the model 1, model 2 and model 3.


***Analyze of data about atherosclerosis***


**Question 1** 

Analyze the impact of the different risk factors on the atherosclerosis.

**Question 2**

Normality of the residuals with shapiro wilk test.

**Question 3** 

Visualization's check of homoscedasticity and residuals normal distribution.

**Question 4** 

Test the auto-correlation of the residuals.

**Question 5**

Influential points.

**Question 6**

Analyze the model quality and what variables bring to the model.




Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2020


















